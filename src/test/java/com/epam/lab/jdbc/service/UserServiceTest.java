package com.epam.lab.jdbc.service;

import com.epam.lab.jdbc.dao.UserDao;
import com.epam.lab.jdbc.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.sql.SQLException;

import static org.mockito.Mockito.*;

public class UserServiceTest {

    private static final Logger LOG = LogManager.getLogger(UserServiceTest.class);

    @InjectMocks
    private UserService unit;

    @Mock
    private UserDao userDao;

    @BeforeTest
    public void setUp() {
        LOG.info("Setuping mocks");
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createSampleUserTest() throws SQLException {
        LOG.info("Mock userDao");
        when(userDao.create(any(User.class))).thenReturn(1);
        LOG.info("Call createSampleUser");
        unit.createSampleUser();
        LOG.info("Verify create method was called");
        verify(userDao, times(1)).create(any(User.class));
    }
}
