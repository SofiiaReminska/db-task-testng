package com.epam.lab.jdbc.dao;

import com.epam.lab.jdbc.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class UserDaoTest {

    private static final String INSERT_INTO_USER_QUERY = "INSERT INTO user(name,surname,email,password,phone,rate) VALUES (?,?,?,?,?,?)";

    private static final Logger LOG = LogManager.getLogger(UserDaoTest.class);

    @InjectMocks
    private UserDao unit;

    @Mock
    private Connection connection;
    @Mock
    private PreparedStatement statement;

    private User dummyUser;

    {
        dummyUser = new User.UserBuilder()
                .setName("Dummy name")
                .setSurname("Dummy surname")
                .setPassword("pass")
                .setPhone("123")
                .setEmail("dummy@dummy.com")
                .build();
        ;
    }
    @BeforeTest
    public void setUp() {
        LOG.info("Setuping mocks");
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void createTest() throws SQLException {
        LOG.info("Mock connection");
        when(connection.prepareStatement(INSERT_INTO_USER_QUERY)).thenReturn(statement);
        LOG.info("Mock statement");
        when(statement.executeUpdate()).thenReturn(1);
        LOG.info("Verify method create method returned 1");
        assertEquals(1, unit.create(dummyUser));
    }
}
