package com.epam.lab.jdbc.model;

public class Car {
    private Integer id;
    private String model;
    private String number;
    private String color;

    public Car() {
    }

    public Car(Integer id, String model, String number, String color) {
        this.id = id;
        this.model = model;
        this.number = number;
        this.color = color;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", model='" + model + '\'' +
                ", number='" + number + '\'' +
                ", color='" + color + '\'' +
                '}';
    }

    public static class CarBuilder {
        private Integer id;
        private String model;
        private String number;
        private String color;

        public CarBuilder setId(Integer id) {
            this.id = id;
            return this;
        }

        public CarBuilder setModel(String model) {
            this.model = model;
            return this;
        }

        public CarBuilder setNumber(String number) {
            this.number = number;
            return this;
        }

        public CarBuilder setColor(String color) {
            this.color = color;
            return this;
        }

        public Car build() {
            return new Car(id, model, number, color);
        }
    }
}
