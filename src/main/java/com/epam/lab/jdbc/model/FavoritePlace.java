package com.epam.lab.jdbc.model;

public class FavoritePlace {
    private Integer id;
    private String name;
    private Integer userId;
    private Integer locationId;

    public FavoritePlace() {
    }

    public FavoritePlace(Integer id, String name, Integer userId, Integer locationId) {
        this.id = id;
        this.name = name;
        this.userId = userId;
        this.locationId = locationId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    @Override
    public String toString() {
        return "FavoritePlace{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", userId=" + userId +
                ", locationId=" + locationId +
                '}';
    }
}
