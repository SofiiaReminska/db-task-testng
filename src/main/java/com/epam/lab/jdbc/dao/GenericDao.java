package com.epam.lab.jdbc.dao;

import java.sql.SQLException;
import java.util.List;

public interface GenericDao<T, K> {
    List<T> getAll() throws SQLException;

    T getByPK(K id) throws SQLException;

    int create(T obj) throws SQLException;

    void update(T obj) throws SQLException;

    void delete(Integer id) throws SQLException;
}

