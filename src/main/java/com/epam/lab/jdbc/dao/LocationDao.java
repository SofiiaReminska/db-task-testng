package com.epam.lab.jdbc.dao;

import com.epam.lab.jdbc.model.Location;
import com.epam.lab.jdbc.transformer.LocationTransformer;

import java.sql.*;
import java.util.List;

public class LocationDao implements GenericDao<Location, Integer> {
    private static final String SELECT_ALL_FROM_LOCATION_QUERY = "SELECT * FROM location";
    private static final String SELECT_ALL_FROM_LOCATION_BY_ID_QUERY = "SELECT * FROM location WHERE id = ?";
    private static final String INSERT_INTO_LOCATION_QUERY = "INSERT INTO location(id,longitude,latitude) VALUES (?,?,?)";
    private static final String UPDATE_LOCATION_QUERY = "UPDATE location SET longitude=?,latitude=? WHERE id=?";
    private static final String DELETE_FROM_LOCATION_BY_ID_QUERY = "DELETE FROM location WHERE id=?";
    private Connection connection;
    private LocationTransformer transformer;

    public LocationDao(Connection connection) {
        this.connection = connection;
        this.transformer = new LocationTransformer();
    }

    @Override
    public List<Location> getAll() throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(SELECT_ALL_FROM_LOCATION_QUERY);
        List<Location> ret = transformer.transformResultSetToList(rs);
        rs.close();
        statement.close();
        return ret;
    }

    @Override
    public Location getByPK(Integer id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_ALL_FROM_LOCATION_BY_ID_QUERY);
        statement.setInt(1, id);
        ResultSet rs = statement.executeQuery();
        Location ret = transformer.transformResultSetToObject(rs);
        rs.close();
        statement.close();
        return ret;
    }

    @Override
    public int create(Location obj) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                INSERT_INTO_LOCATION_QUERY);
        statement.setInt(1, obj.getId());
        statement.setDouble(2, obj.getLongitude());
        statement.setDouble(3, obj.getLatitude());
        return statement.executeUpdate();
    }

    @Override
    public void update(Location obj) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                UPDATE_LOCATION_QUERY);
        statement.setDouble(1, obj.getLongitude());
        statement.setDouble(2, obj.getLatitude());
        statement.setInt(3,obj.getId());
        statement.executeUpdate();
    }

    @Override
    public void delete(Integer id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE_FROM_LOCATION_BY_ID_QUERY);
        statement.setInt(1, id);
        statement.execute();
    }
}
