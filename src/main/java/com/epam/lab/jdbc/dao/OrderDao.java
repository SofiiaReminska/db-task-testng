package com.epam.lab.jdbc.dao;

import com.epam.lab.jdbc.model.Order;
import com.epam.lab.jdbc.transformer.OrderTransformer;

import java.sql.*;
import java.util.List;

public class OrderDao implements GenericDao<Order, Integer> {
    private static final String SELECT_ALL_FROM_ORDER_QUERY = "SELECT * FROM order";
    private static final String SELECT_FROM_ORDER_BY_ID_QUERY = "SELECT * FROM order WHERE id = ?";
    private static final String INSERT_INTO_ORDER_QUERY = "INSERT INTO order(id,driver_rate,distance,datetime,price,user_id,driver_id,departure_location_id,destination_location_id,car_id) VALUES (?,?,?,?,?,?,?,?,?,?)";
    private static final String UPDATE_ORDER_QUERY = "UPDATE order SET driver_rate=?,distance=?,datetime=?,price=?,user_id=?,driver_id=?,departure_location_id=?,destination_location_id=?,car_id=? WHERE id=?";
    private static final String DELETE_FROM_ORDER_BY_ID_QUERY = "DELETE FROM order WHERE id=?";
    private Connection connection;
    private OrderTransformer transformer;

    public OrderDao(Connection connection) {
        this.connection = connection;
        this.transformer = new OrderTransformer();
    }

    @Override
    public List<Order> getAll() throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(SELECT_ALL_FROM_ORDER_QUERY);
        List<Order> ret = transformer.transformResultSetToList(rs);
        rs.close();
        statement.close();
        return ret;
    }

    @Override
    public Order getByPK(Integer id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(SELECT_FROM_ORDER_BY_ID_QUERY);
        statement.setInt(1, id);
        ResultSet rs = statement.executeQuery();
        Order ret = transformer.transformResultSetToObject(rs);
        rs.close();
        statement.close();
        return ret;
    }

    @Override
    public int create(Order obj) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                INSERT_INTO_ORDER_QUERY);
        statement.setInt(1, obj.getId());
        statement.setDouble(2, obj.getDriverRate());
        statement.setDouble(3, obj.getDistance());
        statement.setTimestamp(4, obj.getDatetime());
        statement.setBigDecimal(5, obj.getPrice());
        statement.setInt(6, obj.getUserId());
        statement.setInt(7, obj.getDriverId());
        statement.setInt(8, obj.getDepartureLocationId());
        statement.setInt(9, obj.getDestinationLocationId());
        statement.setInt(10, obj.getCarId());
        return statement.executeUpdate();
    }

    @Override
    public void update(Order obj) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(
                UPDATE_ORDER_QUERY);
        statement.setDouble(1, obj.getDriverRate());
        statement.setDouble(2, obj.getDistance());
        statement.setTimestamp(3, obj.getDatetime());
        statement.setBigDecimal(4, obj.getPrice());
        statement.setInt(5, obj.getUserId());
        statement.setInt(6, obj.getDriverId());
        statement.setInt(7, obj.getDepartureLocationId());
        statement.setInt(8, obj.getDestinationLocationId());
        statement.setInt(9, obj.getCarId());
        statement.setInt(10, obj.getId());
        statement.executeUpdate();
    }

    @Override
    public void delete(Integer id) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(DELETE_FROM_ORDER_BY_ID_QUERY);
        statement.setInt(1, id);
        statement.execute();
    }
}
