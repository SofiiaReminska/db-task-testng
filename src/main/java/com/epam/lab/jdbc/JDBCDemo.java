package com.epam.lab.jdbc;

import com.epam.lab.jdbc.service.CarService;
import com.epam.lab.jdbc.service.DriverService;
import com.epam.lab.jdbc.service.MetaDataService;
import com.epam.lab.jdbc.service.UserService;
import com.epam.lab.jdbc.util.ConnectionManager;

import java.sql.SQLException;

class JDBCDemo {
    private UserService userService = new UserService();
    private DriverService driverService = new DriverService();
    private CarService carService = new CarService();
    private MetaDataService metaDataService = new MetaDataService();

    void runDemo() throws SQLException {
        userService.createSampleUser();
        driverService.createSampleDriver();
        carService.createSampleCar();
        metaDataService.printMetaData();
        ConnectionManager.closeConnection();
    }
}
