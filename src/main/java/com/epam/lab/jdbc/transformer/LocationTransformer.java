package com.epam.lab.jdbc.transformer;

import com.epam.lab.jdbc.model.Location;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.epam.lab.jdbc.Constants.*;

public class LocationTransformer implements GenericTransformer<Location> {
    @Override
    public List<Location> transformResultSetToList(ResultSet rs) throws SQLException {
        if (!rs.isBeforeFirst())
            return null;
        List<Location> ret = new ArrayList<Location>();
        while (rs.next()) {
            Integer id = rs.getInt(LOCATION_ID);
            Double longitude = rs.getDouble(LOCATION_LONGITUDE);
            Double latitude = rs.getDouble(LOCATION_LATITUDE);
            ret.add(new Location(id, longitude, latitude));
        }
        return ret;
    }

    @Override
    public Location transformResultSetToObject(ResultSet rs) throws SQLException {
        if (!rs.isBeforeFirst())
            return null;
        rs.next();
        Integer id = rs.getInt(LOCATION_ID);
        Double longitude = rs.getDouble(LOCATION_LONGITUDE);
        Double latitude = rs.getDouble(LOCATION_LATITUDE);
        return new Location(id, longitude, latitude);
    }
}
